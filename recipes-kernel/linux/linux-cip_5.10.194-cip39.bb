#
# CIP Core, generic profile
#
# Copyright (c) Siemens AG, 2021-2023
#
# Authors:
#  Jan Kiszka <jan.kiszka@siemens.com>
#
# SPDX-License-Identifier: MIT
#

require linux-cip-common.inc

SRC_URI[sha256sum] = "39fa92032d1c5abfd6cc2a04d3dd4a5ab52df7b92a18bb63a3f4424e3c2d9c8b"
